/*
* ACTION
*/
export const PLACE = "PLACE";
export const RESTART = "RESTART";

export function place(y, x) {
  return {
    type: PLACE,
    y: y,
    x: x
  };
}

export function restart() {
  return {
    type: RESTART
  }
}
