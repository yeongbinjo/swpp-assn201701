import { PLACE, RESTART } from '../actions';
import { combineReducers } from 'redux';


export const SIZE = 19;

function makeBoard(size) {
  var board = []
  for (var i = 0; i < size; i++) {
    board[i] = [];
    for (var j = 0; j < size; j++) {
      board[i][j] = '-';
    }
  }
  return board;
}

const initial = {
  board: makeBoard(SIZE),
  status: 'Next O',
  turn: 'O',
  progress: true
}

function checkValidForPlace(state, y, x) {
  if (y < 0 || y >= SIZE || x < 0 || x >= SIZE) return false;
  if (state.board[y][x] != '-') return false;
  if (!state.progress) return false;
  return true;
}

function checkWin(board, turn, y, x) {
  // 가로
  let curX = x, curY = y, cnt = 0;
  while (curX > 0 && board[curY][curX-1] == turn) curX--;
  while (curX < SIZE && board[curY][curX++] == turn) cnt++;
  if (cnt >= 5) return true;
  // 세로
  curX = x, curY = y, cnt = 0;
  while (curY > 0 && board[curY-1][curX] == turn) curY--;
  while (curY < SIZE && board[curY++][curX] == turn) cnt++;
  if (cnt >= 5) return true;
  // \대각
  curX = x, curY = y, cnt = 0;
  while (curX > 0 && curY > 0 && board[curY-1][curX-1] == turn) {
    curX--;
    curY--;
  }
  while (curX < SIZE && curY < SIZE && board[curY++][curX++] == turn) cnt++;
  if (cnt >= 5) return true;
  // /대각
  curX = x, curY = y, cnt = 0;
  while (curX < SIZE - 1 && curY > 0 && board[curY-1][curX+1] == turn) {
    curX++;
    curY--;
  }
  while (curX >= 0 && curY < SIZE && board[curY++][curX--] == turn) cnt++;
  if (cnt >= 5) return true;

  return false;
}

const omak = (state = initial, action) => {
  switch(action.type) {
    case PLACE:
      if (!checkValidForPlace(state, action.y, action.x)) return state;
      let newBoard = JSON.parse(JSON.stringify(state.board));
      newBoard[action.y][action.x] = state.turn;
      const nextTurn = state.turn == 'O' ? 'X' : 'O';
      let status, progress = true;
      if (checkWin(newBoard, state.turn, action.y, action.x)) {
        status = state.turn + ' win';
        progress = false;
      } else {
        status = 'Next ' + nextTurn;
      }
      return {
        board: newBoard,
        turn: nextTurn,
        status: status,
        progress: progress
      };
    case RESTART:
      return initial;
    default:
      return state;
  }
}

const omakReducer = combineReducers({
  omak
});

export default omakReducer;
