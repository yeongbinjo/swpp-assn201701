import React from 'react';
import Cell from './Cell';


class Row extends React.Component {
  render() {
    return (
      <div>
        {this.props.row.map((cell, colIndex) => {
          return (
            <Cell cell={cell}
              rowIndex={this.props.rowIndex}
              colIndex={colIndex}
              key={colIndex} />
          );
        })}
      </div>
    );
  }
}

export default Row;
