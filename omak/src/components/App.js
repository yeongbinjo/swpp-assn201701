import React from 'react';
import { connect } from 'react-redux';
import { restart } from '../actions';
import Board from './Board';


class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <button id="restart" onClick={ this.props.onRestart }>RESTART</button>
        <Board />
        <div id="status_label">{this.props.status}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    status: state.omak.status
  };
}

let mapDispatchToProps = (dispatch) => {
  return {
    onRestart: () => dispatch(restart()),
  }
}

App = connect(mapStateToProps, mapDispatchToProps)(App);

export default App;
