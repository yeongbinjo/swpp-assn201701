import React from 'react';
import { connect } from 'react-redux';
import Row from './Row';


class Board extends React.Component {
  render() {
    const board = this.props.board;
    return (
      <div>
        {board.map((row, rowIndex) => {
          return (
            <Row row={row} rowIndex={rowIndex} key={rowIndex} />
          );
        })}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    board: state.omak.board
  };
}

Board = connect(mapStateToProps)(Board);

export default Board;
