import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import App from './components/App';
import omakReducer from './reducers';

const store = createStore(omakReducer);
const appElement = document.getElementById('app');

ReactDOM.render(
  <Provider store = {store}>
    <App />
  </Provider>,
  appElement
);
