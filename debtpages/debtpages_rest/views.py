from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import viewsets

from debtpages_rest.models import Debt
from debtpages_rest.permissions import DebtPermission, IsOwnerOrDebtAdmin
from debtpages_rest.serializers import DebtSerializer, UserSumSerializer, UserSerializer


class DebtViewSet(viewsets.ModelViewSet):
    queryset = Debt.objects.all()
    serializer_class = DebtSerializer
    permission_classes = [permissions.IsAuthenticated, DebtPermission]

    def perform_create(self, serializer):
        serializer.save(borrower=self.request.user)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrDebtAdmin]


class UserSumViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSumSerializer
    permission_classes = [permissions.IsAuthenticated, IsOwnerOrDebtAdmin]
