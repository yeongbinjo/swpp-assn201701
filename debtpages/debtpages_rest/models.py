from django.contrib.auth.models import User
from django.db import models


class Debt(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    amount = models.PositiveIntegerField()
    borrower = models.ForeignKey(User, related_name='borrower')
    lender = models.ForeignKey(User, related_name='lender')
