from django.contrib import admin

from debtpages_rest.models import Debt

admin.site.register(Debt)
