from django.contrib.auth.models import User
from django.db.models import Sum
from rest_framework import serializers

from debtpages_rest.models import Debt


class DebtSerializer(serializers.ModelSerializer):
    borrower = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)

    class Meta:
        model = Debt
        fields = ('id', 'created', 'amount', 'borrower', 'lender')


class UserSerializer(serializers.ModelSerializer):
    debts_as_borrower = serializers.SerializerMethodField()
    debts_as_lender = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'debts_as_borrower', 'debts_as_lender')

    def get_debts_as_borrower(self, obj):
        return map(lambda x: x.id, Debt.objects.filter(borrower=obj))

    def get_debts_as_lender(self, obj):
        return map(lambda x: x.id, Debt.objects.filter(lender=obj))


class UserSumSerializer(serializers.ModelSerializer):
    lended_money = serializers.SerializerMethodField()
    borrowed_money = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'lended_money', 'borrowed_money')

    def get_lended_money(self, obj):
        return Debt.objects.filter(lender=obj).aggregate(Sum('amount'))['amount__sum']

    def get_borrowed_money(self, obj):
        return Debt.objects.filter(borrower=obj).aggregate(Sum('amount'))['amount__sum']
