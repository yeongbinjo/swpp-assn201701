# coding: utf-8
from rest_framework import permissions


class DebtPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return request.path != '/debts/' or request.user.username == 'debt_admin'
        elif request.method in ['POST', 'DELETE', 'PUT']:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return obj.borrower == request.user or obj.lender == request.user
        elif request.method == 'DELETE':
            return obj.lender == request.user
        elif request.method == 'PUT':
            return request.user.username == 'debt_admin'
        return False


class IsOwnerOrDebtAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.path not in ['/users/', '/usersum/'] or request.user.username == 'debt_admin'

    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return obj == request.user or request.user.username == 'debt_admin'

