"""omok_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from game.views import room_history_view, RoomDetailView, RoomListView, UserViewSet, room_board_view
from game.views import room_player_view

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^rooms/(?P<pk>[0-9]+)/players$', room_player_view),
    url(r'^rooms/(?P<pk>[0-9]+)/history$', room_history_view),
    url(r'^rooms/(?P<pk>[0-9]+)/board$', room_board_view),
    url(r'^rooms/(?P<pk>[0-9]+)/$', RoomDetailView.as_view()),
    url(r'^rooms/', RoomListView.as_view()),
]
