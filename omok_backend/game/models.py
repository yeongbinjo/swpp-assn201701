# coding: utf-8
import json

from django.contrib.auth.models import User
from django.db import models


class Room(models.Model):
    turn = models.IntegerField(default=1)           # 1: player1(O), 2: player2(X)
    win = models.IntegerField(default=0)            # 0: progress, 1: player1, 2: player2
    player1 = models.ForeignKey(User, null=True, blank=True, related_name='room_player1')
    player2 = models.ForeignKey(User, null=True, blank=True, related_name='room_player2')


BOARD_SIZE = 19


def get_initial_board():
    size = BOARD_SIZE
    row = ['-'] * size
    result = []
    for _ in range(size):
        result.append(row)
    return json.dumps(result)


class Board(models.Model):
    room = models.OneToOneField(Room)
    content = models.TextField(default=get_initial_board())    # json

    def get_content_json(self):
        return json.loads(self.content)

    def __check_valid_for_placing(self, turn, y, x):
        if turn != self.room.turn:
            return False
        if y < 0 or y >= BOARD_SIZE or x < 0 or x >= BOARD_SIZE:
            return False
        if self.get_content_json()[y][x] != '-':
            return False
        if self.room.win != 0:
            return False
        return True

    def __check_win(self, turn, y, x):
        con = self.get_content_json()
        # 가로
        cur_x = x
        cur_y = y
        cnt = 0
        while cur_x > 0 and con[cur_y][cur_x-1] == turn:
            cur_x -= 1
        while cur_x < BOARD_SIZE and con[cur_y][cur_x] == turn:
            cur_x += 1
            cnt += 1
        if cnt >= 5:
            return True

        # 세로
        cur_x = x
        cur_y = y
        cnt = 0
        while cur_y > 0 and con[cur_y-1][cur_x] == turn:
            cur_y -= 1
        while cur_y < BOARD_SIZE and con[cur_y][cur_x] == turn:
            cur_y += 1
            cnt += 1
        if cnt >= 5:
            return True

        # \대각
        cur_x = x
        cur_y = y
        cnt = 0
        while cur_x > 0 and cur_y > 0 and con[cur_y-1][cur_x-1] == turn:
            cur_x -= 1
            cur_y -= 1
        while cur_x < BOARD_SIZE and cur_y < BOARD_SIZE and con[cur_y][cur_x] == turn:
            cur_x += 1
            cur_y += 1
            cnt += 1
        if cnt >= 5:
            return True

        # /대각
        cur_x = x
        cur_y = y
        cnt = 0
        while cur_x < BOARD_SIZE - 1 and cur_y > 0 and con[cur_y-1][cur_x+1] == turn:
            cur_x += 1
            cur_y -= 1
        while cur_x >= 0 and cur_y < BOARD_SIZE and con[cur_y][cur_x] == turn:
            cur_x -= 1
            cur_y += 1
            cnt += 1
        if cnt >= 5:
            return True

        return False

    def place(self, turn, y, x):
        if self.__check_valid_for_placing(turn, y, x):
            con = self.get_content_json()
            con[y][x] = turn
            self.content = json.dumps(con)
            self.save()
        else:
            raise ValueError

        room = Room.objects.get(pk=self.room.pk)
        if self.__check_win(turn, y, x):
            room.win = turn

        room.turn = 1 if self.room.turn == 2 else 2
        room.save()


class History(models.Model):
    room = models.ForeignKey(Room)
    player = models.ForeignKey(User)
    place_i = models.IntegerField()
    place_j = models.IntegerField()
