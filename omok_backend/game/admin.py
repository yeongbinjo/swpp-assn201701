from django.contrib import admin

from game.models import Room, History, Board

admin.site.register(Room)
admin.site.register(History)
admin.site.register(Board)
