from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from game.models import Room, History, Board
from game.permissions import RoomsPermission
from game.serializers import RoomSerializer, PlayerSerializer, HistorySerializer, UserSerializer, BoardSerializer


@api_view(['GET'])
def room_board_view(request, pk):
    try:
        room = Room.objects.get(pk=pk)
    except Room.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    board, _ = Board.objects.get_or_create(room=room)
    return Response(BoardSerializer(board).data)


class RoomListView(generics.ListCreateAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (permissions.AllowAny, RoomsPermission)


class RoomDetailView(generics.RetrieveAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (permissions.AllowAny,)


@api_view(['GET', 'POST'])
def room_player_view(request, pk):
    try:
        room = Room.objects.get(pk=pk)
    except Room.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = PlayerSerializer(room)
    if request.method == 'GET':
        return Response(serializer.data)
    elif request.method == 'POST':
        if not request.user.is_authenticated:
            return Response(status=status.HTTP_403_FORBIDDEN)
        elif not room.player1:
            room.player1 = request.user
            room.save()
        elif not room.player2 and room.player1 != request.user:
            room.player2 = request.user
            room.save()
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)
        return Response(status=status.HTTP_201_CREATED)


@api_view(['GET', 'POST'])
def room_history_view(request, pk):
    try:
        room = Room.objects.get(pk=pk)
    except Room.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        return Response(map(lambda x: HistorySerializer(x).data, History.objects.filter(room=room)))
    elif request.method == 'POST':
        if room.turn == 1 and not room.player1 == request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)
        if room.turn == 2 and not room.player2 == request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)
        board, _ = Board.objects.get_or_create(room=room)
        try:
            board.place(room.turn, request.data['place_i'], request.data['place_j'])
        except ValueError:
            return Response(status=status.HTTP_403_FORBIDDEN)
        History.objects.create(
            room=room,
            player=request.user,
            **request.data
        )
        return Response(status=status.HTTP_201_CREATED)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.AllowAny]
