import json

from django.contrib.auth.models import User
from rest_framework import serializers

from game.models import Room, History, Board


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('turn', 'win', 'player1', 'id', 'player2')


class BoardSerializer(serializers.ModelSerializer):
    board_content = serializers.SerializerMethodField()

    class Meta:
        model = Board
        fields = ('room', 'board_content')

    def get_board_content(self, obj):
        return json.loads(obj.content)


class PlayerSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        l1 = ([instance.player1.id] if instance.player1 else [])
        l2 = ([instance.player2.id] if instance.player2 else [])
        return l1 + l2

    class Meta:
        model = Room


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('place_i', 'place_j', 'player')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')
