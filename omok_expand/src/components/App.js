import React from 'react';
import { connect } from 'react-redux';
import { postEnterRoomRequest } from '../actions';
import Board from './Board';


class App extends React.Component {
  constructor(props) {
    super(props);

    this.onClickConnect = this.onClickConnect.bind(this);
  }

  render() {
    return (
      <div>
        <div>
          <input type="text" id="username_field" placeholder="username" />
          <input type="password" id="password_field" placeholder="password" />
          <input type="text" id="room_field" placeholder="room" />
          <button id="connect" onClick={ this.onClickConnect }>connect</button>
        </div>
        <div>
          Enemy:
          <span id="enemy_field">{this.props.enemyname}</span>
        </div>
        <Board />
        <div id="status_label">{this.props.status}</div>
      </div>
    );
  }

  onClickConnect() {
    this.props.onConnect(
      document.querySelector('#username_field').value,
      document.querySelector('#password_field').value,
      document.querySelector('#room_field').value
    );
  }
}

let mapStateToProps = (state) => {
  return {
    status: state.omok.status,
    enemyname: state.omok.enemyname
  };
}

let mapDispatchToProps = (dispatch) => {
  return {
    onConnect: (uname, upwd, rid) => dispatch(postEnterRoomRequest(uname, upwd, rid)),
  }
}

App = connect(mapStateToProps, mapDispatchToProps)(App);

export default App;
