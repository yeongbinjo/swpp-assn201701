import React from 'react';
import { connect } from 'react-redux';
import { sendHistory } from '../actions/';
import { SIZE } from '../reducers/';


class Cell extends React.Component {
  constructor(props) {
    super(props);

    this.onClickCell = this.onClickCell.bind(this);
  }

  renderStone(cell) {
    if (cell == 1) {
      return (
        <div className="stone black"></div>
      );
    } else if (cell == 2) {
      return (
        <div className="stone white"></div>
      );
    }
  }

  renderDot(r, c) {
    if ((r == 3 && c == 3) || (r == 15 && c == 3) || (r == 9 && c == 3) ||
      (r == 3 && c == 9) || (r == 9 && c == 9) || (r == 15 && c == 9) ||
      (r == 3 && c == 15) || (r == 9 && c == 15) || (r == 15 && c == 15)) {
        return <div className="dot"></div>;
    }
  }

  renderCell(cell) {
    const c = cell == '-' ? cell : cell == 1 ? 'O' : 'X';
    return (
      <span>
        {cell}
      </span>
    );
  }

  render() {
    const {cell, rowIndex, colIndex} = this.props;
    let cls = 'cell';
    if (rowIndex == 0 && colIndex == 0) {
      cls += ' top-left';
    } else if (rowIndex == SIZE - 1 && colIndex == SIZE - 1) {
      cls += ' bottom-right';
    } else if (rowIndex == 0 && colIndex == SIZE - 1) {
      cls += ' top-right';
    } else if (rowIndex == SIZE - 1 && colIndex == 0) {
      cls += ' bottom-left';
    } else if (rowIndex == 0) {
      cls += ' top';
    } else if (rowIndex == SIZE - 1) {
      cls += ' bottom';
    } else if (colIndex == 0) {
      cls += ' left';
    } else if (colIndex == SIZE - 1) {
      cls += ' right';
    } else {
      cls += ' default';
    }
    return (
      <div id={rowIndex + "_" + colIndex} className={cls} onClick={ this.onClickCell }>
        { this.renderCell(cell) }
        { this.renderStone(cell) }
        { this.renderDot(rowIndex, colIndex) }
      </div>
    );
  }

  onClickCell() {
    this.props.onPlace(this.props.rid, this.props.hash,
      this.props.rowIndex, this.props.colIndex);
  }
}

let mapStateToProps = (state) => {
  return {
    rid: state.omok.rid,
    hash: state.omok.hash,
  };
}

let mapDispatchToProps = (dispatch) => {
  return {
    onPlace: (rid, hash, y, x) => dispatch(sendHistory(rid, hash, y, x)),
  }
}

Cell = connect(mapStateToProps, mapDispatchToProps)(Cell);

export default Cell;
