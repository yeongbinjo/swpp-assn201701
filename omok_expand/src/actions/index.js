/*
* ACTION
*/
export const POST_ENTER_ROOM_REQUEST = "POST_ENTER_ROOM_REQUEST";
export const GET_ACCEPT_ENEMY_REQUEST = "GET_ACCEPT_ENEMY_REQUEST";
export const ENTER_ROOM_SUCCESS = "ENTER_ROOM_SUCCESS";
export const START_GAME = "START_GAME";
export const GET_GAME_STATUS = "GET_GAME_STATUS";
export const SET_GAME_STATUS = "SET_GAME_STATUS";
export const SEND_HISTORY = "SEND_HISTORY";

export function sendHistory(rid, hash, y, x) {
  return {
    type: SEND_HISTORY,
    rid: rid,
    hash: hash,
    y: y,
    x: x
  };
}

export function postEnterRoomRequest(uname, upwd, rid) {
  return {
    type: POST_ENTER_ROOM_REQUEST,
    uname: uname,
    upwd: upwd,
    rid: rid
  }
}

export function getAcceptEnemyRequest(rid, uname) {
  return {
    type: GET_ACCEPT_ENEMY_REQUEST,
    rid: rid,
    uname: uname
  }
}

export function enterRoomSuccess(rid, uname, hash) {
  return {
    type: ENTER_ROOM_SUCCESS,
    rid: rid,
    uname: uname,
    hash: hash
  }
}

export function startGame(enemyname, uid) {
  return {
    type: START_GAME,
    enemyname: enemyname,
    uid: uid
  }
}

export function getGameStatus(rid) {
  return {
    type: GET_GAME_STATUS,
    rid: rid
  }
}

export function setGameStatus(turn, win, player1, player2, board) {
  return {
    type: SET_GAME_STATUS,
    turn: turn,
    win: win,
    player1: player1,
    player2: player2,
    board: board
  }
}
