import { START_GAME, SET_GAME_STATUS, ENTER_ROOM_SUCCESS } from '../actions';
import { combineReducers } from 'redux';


export const SIZE = 19;

function makeBoard(size) {
  var board = []
  for (var i = 0; i < size; i++) {
    board[i] = [];
    for (var j = 0; j < size; j++) {
      board[i][j] = '-';
    }
  }
  return board;
}

const initial = {
  rid: 0,
  board: makeBoard(SIZE),
  status: '',
  turn: 1,
  win: 0,
  uid: 0,
  hash: '',
  enemyname: '',
  progress: false,
}

function checkValidForPlace(state, y, x) {
  if (y < 0 || y >= SIZE || x < 0 || x >= SIZE) return false;
  if (state.board[y][x] != '-') return false;
  if (!state.progress) return false;
  return true;
}

function getStatusMessage(state) {
  if (state.hash == '') return '';
  else if (state.enemyname == '') return 'WAITING';
  else if (state.win == 1) return 'O Win';
  else if (state.win == 2) return 'X Win';
  else if (state.turn == 1) return 'Next O';
  else if (state.turn == 2) return 'Next X';
  else return '';
}

const omok = (state = initial, action) => {
  let copy;
  switch(action.type) {
    case ENTER_ROOM_SUCCESS:
      copy = Object.assign({}, initial);
      copy.rid = action.rid;
      copy.hash = action.hash;
      copy.status = getStatusMessage(copy);
      return copy;
    case START_GAME:
      copy = Object.assign({}, state);
      copy.enemyname = action.enemyname;
      copy.uid = action.uid;
      copy.status = getStatusMessage(copy);
      return copy;
    case SET_GAME_STATUS:
      copy = Object.assign({}, state);
      copy.board = action.board;
      copy.turn = action.turn;
      copy.win = action.win;
      copy.status = getStatusMessage(copy);
      let progress = false;
      if (action.player1 && action.player2) {
        if (copy.turn == 1) {
          progress = state.uid == action.player1;
        } else if (copy.turn == 2) {
          progress = state.uid == action.player2;
        }
      }
      copy.progress = progress;
      return copy;
    default:
      return state;
  }
}

const omokReducer = combineReducers({
  omok
});

export default omokReducer;
