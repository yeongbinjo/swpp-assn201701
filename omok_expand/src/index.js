import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga'
import App from './components/App';
import omokReducer from './reducers';
import mySaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(omokReducer, applyMiddleware(sagaMiddleware));
const appElement = document.getElementById('app');

sagaMiddleware.run(mySaga);
ReactDOM.render(
  <Provider store = {store}>
    <App />
  </Provider>,
  appElement
);
