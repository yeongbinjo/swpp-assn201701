import 'whatwg-fetch';
import { delay } from 'redux-saga';
import { spawn, fork, call, put, take } from 'redux-saga/effects';
import { getAcceptEnemyRequest, enterRoomSuccess, startGame, getGameStatus, setGameStatus } from './actions';

function players_url(rid) {
  return `http://localhost:8000/rooms/${rid}/players`;
}

function user_url(uid) {
  return `http://localhost:8000/users/${uid}/`;
}

function room_url(rid) {
  return `http://localhost:8000/rooms/${rid}/`;
}

function board_url(rid) {
  return `http://localhost:8000/rooms/${rid}/board`;
}

function history_url(rid) {
  return `http://localhost:8000/rooms/${rid}/history`;
}

function* watchHistory() {
  while(true) {
    const data = yield take('SEND_HISTORY');
    yield call(postHistory, data);
  }
}

function* postHistory(data) {
  const {rid, hash, y, x} = data;
  const response = yield call(fetch, history_url(rid), {
    method: 'POST',
    headers: {
      'Authorization': `Basic ${hash}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      place_i: y,
      place_j: x,
    })
  });
  yield call(acceptGameStatus, data);
}

function* watchLogin() {
  const data = yield take('POST_ENTER_ROOM_REQUEST');
  yield call(postEnterRoom, data);
}

function* postEnterRoom(data) {
  const {uname, upwd, rid} = data;
  const hash = new Buffer(`${uname}:${upwd}`).toString('base64');
  const response = yield call(fetch, players_url(rid), {
    method: 'POST',
    headers: {
      'Authorization': `Basic ${hash}`
    }
  });
  if (!response.ok) {
    // yield put(enterRoomFail());
  } else {
    yield put(enterRoomSuccess(rid, uname, hash));
  }
}

function* createEnemyWait() {
  const data = yield take('ENTER_ROOM_SUCCESS');
  yield put(getAcceptEnemyRequest(data.rid, data.uname));
}

function* waitEnemy() {
  while(true) {
    const data = yield take('GET_ACCEPT_ENEMY_REQUEST');
    yield call(acceptEnemy, data);
  }
}

function* watchGameStatus() {
  while(true) {
    const data = yield take('GET_GAME_STATUS');
    yield call(acceptGameStatus, data);
  }
}

function* acceptGameStatus(data) {
  const { rid } = data;

  const res = yield call(get, room_url(rid));
  const bres = yield call(get, board_url(rid));
  yield put(setGameStatus(
    res['turn'], res['win'],
    res['player1'], res['player2'],
    bres['board_content'],
  ));
  if (res['win'] == 0) {
    yield spawn(createNextGameStatus, {rid});
  }
}

function* createNextGameStatus(data) {
  const {rid} = data;
  yield delay(1000);
  yield put(getGameStatus(rid));
}

const parseJSON = response => response.json();

function get(url) {
  return fetch(url, {method: 'GET'}).then(parseJSON);
}

function* acceptEnemy(data) {
  const { rid, uname } = data;

  const res = yield call(get, players_url(rid));
  let users = [];
  for (var i = 0; i < res.length; i++) {
    const ires = yield call(get, user_url(res[i]));
    users.push(ires);
  }

  if (users.length == 2) {
    let uid = 0;
    let enemyname = '';
    if (users[0]['username'] == uname) {
      enemyname = users[1]['username'];
      uid = users[0]['id'];
    } else {
      enemyname = users[0]['username'];
      uid = users[1]['id'];
    }
    yield put(startGame(enemyname, uid));
    yield put(getGameStatus(rid));
  } else {
    yield spawn(createNextEnemyRequest, {rid, uname});
  }
}

function* createNextEnemyRequest(data) {
  const {rid, uname} = data;
  yield delay(1000);
  yield put(getAcceptEnemyRequest(rid, uname));
}

function* omokSaga() {
  yield fork(watchHistory);
  yield fork(watchLogin);
  yield fork(waitEnemy);
  yield fork(createEnemyWait);
  yield fork(watchGameStatus);
}

export default omokSaga;
